FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-stretch-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443 

FROM mcr.microsoft.com/dotnet/core/sdk:2.2-stretch AS build
WORKDIR /app
 
COPY /*.csproj ./
RUN dotnet restore

COPY . ./
 
FROM build AS publish
RUN dotnet publish -c Release -o out
 
FROM base AS final
COPY --from=publish /app/out .

ENTRYPOINT ["dotnet", "HelloNetCore.dll"]


#docker build -t hello-netcore:1.1 .
#docker run -d -p 7000:80 hello-netcore:1.1 . 