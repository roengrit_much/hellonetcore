﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HelloNetCore.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HelloNetCore.Controller
{
    [ApiController]
    [Route("api/[controller]")]
    public class EchoController : ControllerBase
    {
        // GET: /<controller>/
        [HttpGet]
        public ReturnStatus Get()
        {
            return new ReturnStatus { Status = true, Message = "I'm NetCore." };
        }
    }
}
