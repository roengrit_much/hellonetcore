﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloNetCore.Models
{
    public class ReturnStatus
    {
        public bool Status { get; set; }
        public string Message { get; set; }
    }
}
